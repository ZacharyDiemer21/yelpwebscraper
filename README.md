# **Welcome to the simple Yelp Webscraper** #

Hello! This script can be used to scrap yelp for the business name, business address, phone number, the number of reviews for every zipcode in the country. All you have to do is open up the terminal, or your typical version of python, download the newest version of chrome driver, install some additional python packages for example Selenium, and BeautifulSoup, download the zipcode txt files for the United States, adjust the paths appropriately to these txt files, the chromedriver, etc., adjust the search term to the specific search term you are interested in, copy and paste the script into the terminal, and voila!  

**There have been additional r scripts, txt files, and guidelines for the interactive mapping portion.**

## **Ways to better such a project:**  
* Utilize Apache spark, remove duplicates before writing out to CSV (aka Combine R script and Python Script, can also do with Spark).  

* Fix time out problem, possibly add a try and catch statement for the entire function 

* Clean up and remove inefficiencies among the script 

* Convert the flow of data to dataframes, run transformations and analytics in realtime, then write to a database. 

### What is this repository for? ###

* Quick summary  

In this particular case we are using the search term "vape shops". 

* Version


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact